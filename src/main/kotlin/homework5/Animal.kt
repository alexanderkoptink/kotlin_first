package homework5

abstract class Animal(
    open val name: String,
    open val height: Double,
    open val weight: Double,
    private val foodPreferences: Array<String>,
    private val type: String
) {
    private var satiety = 0
    fun eat(foods: Array<String>) {
        var j = 0
        println("Кормим $type по кличке $name.")
        for (i in foods) {
            val eat: String = foods[j]
            if (eat in foodPreferences) {
                satiety++
                println("$type $name поел $eat. Сытость: $satiety")
            } else {
                println("$type $name не ест $eat. Сытость: $satiety")
            }
            j++
        }
    }
}