package homework5

fun main() {
    println("Кормим животных:")
    val arrayAnimals = arrayOf(
        Lion("Jenya", 22.0, 42.0),
        Tiger("Dima", 32.0, 22.0),
        Slon("Afma", 95.0, 54.0),
        Begemot("Goga", 34.0, 24.0),
        Wolf("Lema", 34.0, 14.0),
        Girafe("Lenya", 33.0, 20.0),
        Shimpanze("Kolya", 43.0, 44.0),
        Gorilla("Vasya", 52.0, 20.0)
    )
    val arrayFoods = arrayOf(
        "Фрукты", "Листья", "Мясо", "Трава", "Орехи", "Овощи", "Рыба", "Кости"
    )
    eatUnits(arrayAnimals, arrayFoods)
}

fun eatUnits(animals: Array<Animal>, foods: Array<String>) {
    for (animal in animals) {
        animal.eat(foods)
    }
}