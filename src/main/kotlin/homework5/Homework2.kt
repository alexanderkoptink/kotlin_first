package homework5

fun main() {
    println("Введите любое целое число:")
    val digit = readLine()!!.toInt()
    println("Перевернутое число: ${reverseNumber(digit)}")
}

fun reverseNumber(digit: Int): Int {
    var num = digit
    var reversedNum = 0
    while (num != 0) {
        val remainder = num % 10
        reversedNum = reversedNum * 10 + remainder
        num /= 10
    }
    return reversedNum
}