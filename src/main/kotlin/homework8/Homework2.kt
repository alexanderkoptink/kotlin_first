package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    val vegetablesCount = countVegetables()
    println("Количество овощей: $vegetablesCount")

    val totalPrice = calculateTotalPrice()
    println("Общая стоимость: $totalPrice")
}

fun countVegetables(): Int {
    var count = 0
    for (entry in userCart.entries) {
        if (entry.key in vegetableSet) {
            count += entry.value
        }
    }
    return count
}

fun calculateTotalPrice(): Double {
    var total = 0.0
    for (entry in userCart.entries) {
        var price = prices[entry.key]
        if (price == null) {
            throw IllegalArgumentException("Товар ${entry.key} отсутствует в списке цен")
        }
        if (entry.key in discountSet) {
            price *= (1 - discountValue)
        }
        total += price * entry.value
    }
    return total
}