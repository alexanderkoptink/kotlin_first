package final_homework_sea_battle

class Game(val player1: Player, val player2: Player) {
    var currentPlayer: Player = player1
    var opponent: Player = player2

    fun start() {
        println("Игра Морской бой для двух игроков начинается!")

        try {
            player1.placeShips()
            player2.placeShips()
        } catch (e: ArrayIndexOutOfBoundsException) {
            println("Ошибка при размещении кораблей. Убедитесь, что корабли размещаются в пределах игрового поля.")
            return
        }

        while (true) {
            if (isGameOver()) {
                println("${currentPlayer.name} победил!")
                break
            }
            else {
                currentPlayer.takeTurn(opponent)
                swapPlayers()
            }
        }
    }

    private fun swapPlayers() {
        val temp = currentPlayer
        currentPlayer = opponent
        opponent = temp
    }

    private fun isGameOver(): Boolean {
        return player1.ships.all { it.isSunk } || player2.ships.all { it.isSunk }
    }
}
