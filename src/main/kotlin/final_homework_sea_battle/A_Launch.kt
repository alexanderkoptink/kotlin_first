package final_homework_sea_battle

fun main() {
    println("Игра Морской бой для двух игроков")

    print("Введите имя первого игрока: ")
    val playerName1 = readLine() ?: "Игрок 1"

    print("Введите имя второго игрока: ")
    var playerName2 = readLine() ?: "Игрок 2"

    // Проверка на уникальность имен
    while (playerName1 == playerName2) {
        println("Имена игроков должны отличаться. Введите уникальное имя для второго игрока: ")
        playerName2 = readLine() ?: "Игрок 2"
    }

    val player1 = Player(playerName1)
    val player2 = Player(playerName2)
    val game = Game(player1, player2)
    game.start()
}
