package final_homework_sea_battle

import java.util.*

class Player(val name: String) {
    val gameBoard = Array(4) { CharArray(4) { EMPTY } }
    val ships = mutableListOf<Ship>()

    companion object {
        const val SHIP_PART = 'O' // Обозначение части корабля
        const val HIT = 'X'       // Обозначение попадания
        const val MISS = '-'      // Обозначение промаха
        const val EMPTY = '.'     // Обозначение пустой клетки
    }

    fun placeShips() {
        for (i in 0 until 2) {
            placeShip(1)
        }
        placeShip(2)
    }

    private fun placeShip(size: Int) {
        println("$name, разместите корабль размером $size клетки.")
        var i = 0
        while (i < size) {
            val (rowIndex, colIndex) = readCoordinate("Введите координаты для палубы ${i + 1} (например, A1): ")

            if (gameBoard[rowIndex][colIndex] == EMPTY && isShipPlacementValid(rowIndex, colIndex, size)) {
                placeShipPart(rowIndex, colIndex, size)
                i++
            } else {
                println("Клетка занята или корабль размещается некорректно! Попробуйте еще раз.")
            }
        }
        ships.add(Ship(size))
        println("$name, корабль размещен успешно.")
        displayRemainingShips()
        printOwnBoard()
    }

    private fun isShipPlacementValid(startRow: Int, startCol: Int, size: Int): Boolean {
        if (startCol + size > gameBoard[0].size) {
            return false
        }

        for (i in 0 until size) {
            if (gameBoard[startRow][startCol + i] == SHIP_PART) {
                return false
            }
        }

        if (size == 2) {

            if (startRow > 0 && gameBoard[startRow - 1][startCol + 1] == SHIP_PART) {
                return false
            }
            if (startRow < gameBoard.size - 1 && gameBoard[startRow + 1][startCol + 1] == SHIP_PART) {
                return false
            }

            if (startCol < gameBoard[0].size - 1 && startCol + 2 < gameBoard[0].size && gameBoard[startRow][startCol + 2] == SHIP_PART) {
                return false
            }
        }

        return true
    }

    private fun placeShipPart(startRow: Int, startCol: Int, size: Int) {

        for (i in 0 until size) {
            gameBoard[startRow][startCol] = SHIP_PART
        }
    }

    fun takeTurn(targetPlayer: Player) {
        if (targetPlayer.ships.all { it.isSunk }) {
            println("${targetPlayer.name} все корабли потоплены!")
            return
        }
        println("$name, ваш ход.")
        printEnemyBoard(targetPlayer)

        print("Введите координаты для выстрела (например, A1, B2): ")
        val input = readlnOrNull()?.uppercase(Locale.getDefault()) ?: ""

        // Проверяем, что введенная строка имеет нужную длину
        if (input.length == 2) {
            val row = input[0]
            val col = input[1]

            // Проверяем, что введенные символы соответствуют ожидаемым значениям
            if (row in 'A'..'D' && col in '1'..'4') {
                val rowIndex = row - 'A'
                val colIndex = col.toString().toInt() - 1

                when (targetPlayer.gameBoard[rowIndex][colIndex]) {
                    SHIP_PART -> {
                        println("Попадание!")
                        targetPlayer.markHit(rowIndex, colIndex)
                        takeTurn(targetPlayer)
                    // Продолжаем ходить до промаха
                    }

                    HIT -> {
                        println("Уже стреляли в эту клетку. Попробуйте еще раз.")
                        takeTurn(targetPlayer)
                    }

                    else -> {
                        println("Промах!")
                        targetPlayer.markMiss(rowIndex, colIndex)
                    }
                }
                return
            }
        }
        println("Некорректный ввод. Попробуйте еще раз.")
        takeTurn(targetPlayer)
    }

    private fun markHit(row: Int, col: Int) {
        gameBoard[row][col] = HIT
        val ship = ships.find { it.size > 0 && !it.isSunk }
        ship?.let {
            it.size -= 1
            if (it.size == 0) {
                it.isSunk = true
                println("$name, вы потопили корабль противника!")
            }
        }
    }

    private fun markMiss(row: Int, col: Int) {
        gameBoard[row][col] = MISS
    }

    private fun printEnemyBoard(enemy: Player) {
        printGameBoard("$name, поле противника:", enemy.gameBoard) { cell ->
            if (cell == SHIP_PART || cell == EMPTY) ". " else "$cell "
        }
    }
    private fun printGameBoard(header: String, board: Array<CharArray>, formatCell: (Char) -> String) {
        println(header)
        println("  1 2 3 4")
        for ((index, row) in board.withIndex()) {
            print("${('A' + index)} ")
            for (cell in row) {
                print(formatCell(cell))
            }
            println()
        }
        println()
    }
    private fun printOwnBoard() {
        printGameBoard(name, gameBoard) { cell -> "$cell " }
    }

    private fun readCoordinate(prompt: String): Pair<Int, Int> {
        while (true) {
            print(prompt)
            val input = readlnOrNull()?.uppercase(Locale.getDefault()) ?: ""
            if (input.length == 2) {
                val row = input[0]
                val col = input[1]

                if (row in 'A'..'D' && col in '1'..'4') {
                    val rowIndex = row - 'A'
                    val colIndex = col.toString().toInt() - 1
                    return Pair(rowIndex, colIndex)
                }
            }

            println("Введите корректные координаты (например: A1, B2).")
        }
    }

    private fun displayRemainingShips() {
        println("$name, осталось кораблей:")
        for (ship in ships) {
            if (!ship.isSunk) {
                println("Корабль размером ${ship.size}")
            }
        }
    }
}



