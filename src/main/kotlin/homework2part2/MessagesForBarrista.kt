package homework2.part2

fun main() {
    val byteGlass: Byte = 50
    val shortGlass: Short = 18500
    val longGlass: Long = 3000000000
    val doubleGlass: Double = 0.666666667
    val intGlass: Int = 200
    val floatGlass: Float = 0.5F
    val stringGlass: String = "Что-то авторское!"
    val byteFormat = "мл виски"
    val shortFormat = "мл лимонада"
    val longFormat = "капель фреша"
    val doubleFormat = "литра эля"
    val intFormat = "мл пина колады"
    val floatFormat = "литра колы"

    println(message = "Напитки в стаканах по порядку:")
    println("Заказ - '$byteGlass $byteFormat' готов!")
    println("Заказ - '$shortGlass $shortFormat' готов!")
    println("Заказ - '$longGlass $longFormat' готов!")
    println("Заказ - '$doubleGlass $doubleFormat' готов!")
    println("Заказ - '$intGlass $intFormat' готов!")
    println("Заказ - '$floatGlass $floatFormat' готов!")
    println("Заказ - '$stringGlass' готов!")
}
