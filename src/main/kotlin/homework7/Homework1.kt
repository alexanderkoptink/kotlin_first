package homework7

fun main() {
    try {
        val user = registerUser("testuser", "password123", "password123")
        println("Юзер ${user.login} успешно зарегистрирован!")
    } catch (e: WrongLoginException) {
        println(e.message)
    } catch (e: WrongPasswordException) {
        println(e.message)
    }
}

fun registerUser(login: String, password: String, repeatPassword: String): User {
    if (login.length > 20) throw WrongLoginException("В логине не должно быть более 20 символов.")
    if (password.length < 10) throw WrongPasswordException("В пароле не должно быть менее 10 символов.")
    if (password != repeatPassword) throw WrongPasswordException("Логин и пароль должны совпадать.")

    return User(login, password)
}
