package homework3

fun main() {
    val myArray = intArrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for (i in 0..myArray.size) {
        var isSwapped = false
        for (j in 0 until myArray.size - 1 - i) {
            if (myArray[j] > myArray[j + 1]) {
                val swap = myArray[j]
                myArray[j] = myArray[j + 1]
                myArray[j + 1] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        println(item)
    }
}