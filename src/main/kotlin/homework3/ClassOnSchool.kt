package homework3

fun main() {
    val listOfSchollars = intArrayOf(2, 3, 4, 5, 3, 3, 5, 5, 3, 4)
    var otlichinki = 0
    var horohisty = 0
    var troeshniki = 0
    var dvoeshniki = 0
    for (i in listOfSchollars) {
        when (i) {
            5 -> otlichinki++
            4 -> horohisty++
            3 -> troeshniki++
            2 -> dvoeshniki++
        }
    }
    println("Отличников - ${(otlichinki.toDouble() / listOfSchollars.size * 100)}%")
    println("Хорошистов - ${(horohisty.toDouble() / listOfSchollars.size * 100)}%")
    println("Троечников - ${(troeshniki.toDouble() / listOfSchollars.size * 100)}%")
    println("Двоечников - ${(dvoeshniki.toDouble() / listOfSchollars.size * 100)}%")
}