package homework3

fun main() {
    val cash = intArrayOf(1, 2, 4, 5, 10, 20, 1, 2, 3, 4, 5)
    var cashJoe = 0
    var cashTeam = 0
    for (i in cash) {
        if (i % 2 == 0) cashJoe++ else cashTeam++
    }
    println("В сундуке ${cash.size} монет")
    println("У Джо $cashJoe монет")
    println("У команды $cashTeam монет")
}