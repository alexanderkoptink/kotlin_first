package homework_2

fun main() {
    val byteGlass: Byte = 50
    val shortGlass: Short = 18500
    val longGlass: Long = 3000000000
    val doubleGlass: Double = 0.666666667
    val intGlass: Int = 200
    val floatGlass: Float = 0.5F
    val stringGlass: String = "Что-то авторское!"
    println("Напитки в стаканах по порядку:")
    println(byteGlass)
    println(shortGlass)
    println(longGlass)
    println(doubleGlass)
    println(intGlass)
    println(floatGlass)
    println(stringGlass)
}